import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsentFormComponent } from './components/consent-form/consent-form.component';
import { ConsentDetailsComponent } from './components/consent-details/consent-details.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

// Creating routes for the application.
const routes: Routes = [
  { path: '', component: ConsentFormComponent },
  { path: 'details', component: ConsentDetailsComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
