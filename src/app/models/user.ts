// User model
export interface User {
    fullName: string,
    email: string,
    consents?: Array<Consent>
}

// Consent model
export interface Consent {
    description: string,
    id: number,
    checked?: boolean
}
