import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Config } from 'protractor';

// User model.
import { User } from '../models/user';

// Header options which cab be used in the request.
const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json',
    'Access-Control-Expose-Headers': '*'
  })
};

@Injectable()
export class UserService {
  // properties
  postURL: string = "http://localhost:3000/consents";

  constructor(private httpClient: HttpClient) { }

  // to fetch page level data.
  getUsers(pageNumber: number, limit: number): Observable<HttpResponse<Config>> {
    const url = `${this.postURL}?_page=${pageNumber}&_limit=${limit}`;
    return this.httpClient.get<Config>(url, { observe: 'response', headers: httpOptions.headers });
  }

  // to save user data.
  addUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.postURL, user, httpOptions);
  }
}
