import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { UserService } from './user.service';
import { User } from '../models/user';

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService],
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.get(UserService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('User service addUser method', () => {
    // initialise
    const sampleUser = {
      fullName: 'Rama',
      email: 'rama@abc.com',
      consents: [{
        id: 1, description: 'test', checked: true
      }]
    };
    let result: User;

    service.addUser(sampleUser).subscribe(user => {
      result = user
    });

    const req = httpMock.expectOne(service.postURL);
    expect(req.request.method).toBe("POST");
    req.flush(sampleUser);

    expect(result).toEqual(sampleUser);
  });

  it('User service getUsers method', () => {
    // initialise
    const sampleUsers = [{
      fullName: 'Rama',
      email: 'rama@abc.com',
      consents: [{
        id: 1, description: 'test', checked: true
      }]
    }];
    let result: User[];
    const url = `${service.postURL}?_page=1&_limit=1`;

    service.getUsers(1,1).subscribe(response => {
      result = response.body as User[];
    });

    const req = httpMock.expectOne(url);
    expect(req.request.method).toBe("GET");
    req.flush(sampleUsers);

    expect(result).toEqual(sampleUsers);
  });
});
