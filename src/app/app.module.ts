import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";

// routing component.
import { AppRoutingModule } from './app-routing.module';

// user defined components.
import { AppComponent } from './app.component';
import { ConsentFormComponent } from './components/consent-form/consent-form.component';
import { ConsentDetailsComponent } from './components/consent-details/consent-details.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

// User service to fetch and store users.
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    ConsentFormComponent,
    ConsentDetailsComponent,
    NavbarComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
