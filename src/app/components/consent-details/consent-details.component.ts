import { Component, OnInit } from '@angular/core';

// User and Consent Model.
import { User, Consent } from '../../models/user';

// User service.
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-consent-details',
  templateUrl: './consent-details.component.html'
})

export class ConsentDetailsComponent implements OnInit {
  // properties.
  users: User[] = [];
  // to store the total records count.
  totalCount: number;
  // stores current page number.
  page: number = 1;
  // stores the limit, number of records per page.
  limit: number = 2;
  // to store the total page count.
  totalPages: number = 1;
  // stores number of pages as array.
  pages: number[] = [];

  // dependency injecting user service
  constructor(private userService: UserService) { }

  // used to concadinate consent description for which the user has agreed.
  getCheckedConsents(consents: Consent[]) {
    let result = '';
    consents.forEach(consent => {
      if (consent.checked) {
        result = result + consent.description + ', ';
      }
    })
    return result;
  }

  // used to fetch next set of records.
  gotoNext() {
    this.getCurrentPageData(this.page + 1);
  }

  // used to fetch the previous set of records.
  gotoPrevious() {
    this.getCurrentPageData(this.page - 1);
  }

  // used to get records for the current page.
  // pass the value of selected page as number.
  getCurrentPageData(selectedPage: number) {
    this.page = selectedPage;
    this.userService.getUsers(selectedPage, this.limit).subscribe((response) => {
      this.users = response.body as User[];
    });
  }

// initial cal to the service to fetch first page details.
  ngOnInit(): void {
    this.userService.getUsers(this.page, this.limit).subscribe((response) => {
      // get the total count from response header.
      this.totalCount = parseInt(response.headers.get("X-Total-Count"), 10);
      this.users = response.body as User[];
      // total pages will be rounded to the nearest integer.
      this.totalPages = Math.ceil(this.totalCount / this.limit);
      // to get number of pages based on totalpages.
      for (let i = 1; i <= this.totalPages; i++) {
        this.pages.push(i);
      }
    });
  }

}
