import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { Config } from 'protractor';

import { ConsentDetailsComponent } from './consent-details.component';
import { User, Consent } from '../../models/user';

describe('ConsentDetailsComponent', () => {
  let component: ConsentDetailsComponent;
  let fixture: ComponentFixture<ConsentDetailsComponent>;
  const sampleUsers: User[] = [{
    fullName: 'Rama',
    email: 'rama@abc.com',
    consents: [{
      id: 1, description: 'test', checked: true
    }]
  }, {
    fullName: 'John',
    email: 'john@abc.com',
    consents: [{
      id: 1, description: 'test', checked: true
    }]
  }];
  const response = new HttpResponse({
    body: sampleUsers,
    headers: new HttpHeaders({ 'x-total-count': '2' })
  });

  // to fake user service.
  class FakeUserService {
    getUsers(pageNumber: number, limit: number): Observable<HttpResponse<Config>> {
      return of(response);
    };
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsentDetailsComponent],
      providers: [{
        provide: UserService,
        useClass: FakeUserService
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentDetailsComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    // act
    fixture.detectChanges();
    //expect
    expect(component).toBeTruthy();
    expect(component.totalCount).toEqual(2);
    expect(component.users).toEqual(sampleUsers);
    expect(component.totalPages).toEqual(1);
    expect(component.pages).toEqual([1]);
  });

  it('contact details component test getCheckedConsents method', () => {
    // initialise
    const sampleConsent: Consent[] = [{
      id: 1, description: 'test', checked: true
    }, {
      id: 2, description: 'test2', checked: null
    }
    ];

    // act
    fixture.detectChanges();
    const result = component.getCheckedConsents(sampleConsent);

    //expect
    expect(result).toEqual('test, ');
  });

  it('contact details component test gotoNext method', () => {
    // act
    fixture.detectChanges();
    component.gotoNext();

    //expect
    expect(component.page).toEqual(2);
    expect(component.users).toEqual(sampleUsers);
  });

  it('contact details component test gotoPrevious method', () => {
    // initialise
    component.page = 2;

    // act
    fixture.detectChanges();
    component.gotoPrevious();

    //expect
    expect(component.page).toEqual(1);
    expect(component.users).toEqual(sampleUsers);
  });

  it('contact details component test getCurrentPageData method', () => {
    // act
    fixture.detectChanges();
    component.getCurrentPageData(3);

    //expect
    expect(component.page).toEqual(3);
    expect(component.users).toEqual(sampleUsers);
  });
});
