import { Component, ViewChild } from '@angular/core';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-consent-form',
  templateUrl: './consent-form.component.html'
})
export class ConsentFormComponent {

  // to store initial user data.
  user: User = {
    fullName: '',
    email: '',
    consents: [
      { id: 1, description: 'Recieve newsletter', checked: false },
      { id: 2, description: 'Be shown targeted ads', checked: false },
      { id: 3, description: 'Contribute to anonymous visit statistics', checked: false }
    ]
  }
  // to store the selected consent values.
  selectedCheckboxes = [];
  // to store the user form.
  @ViewChild('consentForm')
  form: any;

  constructor(private userService: UserService) { }

  // to see whether the user has agreed for any consents.
  checkSelected(): void {
    this.selectedCheckboxes = this.user.consents.filter(checkbox => checkbox.checked);
  }

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    // to store user data.
    this.userService.addUser(this.user).subscribe(users => { });
    // reset form values.
    this.form.reset();
  }
}
