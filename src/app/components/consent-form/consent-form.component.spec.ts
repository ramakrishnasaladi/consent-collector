import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { ConsentFormComponent } from './consent-form.component';
import { User } from '../../models/user';
import { UserService } from 'src/app/services/user.service';

describe('ConsentFormComponent', () => {
  let component: ConsentFormComponent;
  let fixture: ComponentFixture<ConsentFormComponent>;
  let dummyUser: User = {
    fullName: 'Rama',
    email: 'rama@abc.com',
    consents: [{
      id: 1, description: 'test', checked: true
    }]
  };

  // to fake user service.
  class FakeUserService {
    addUser(user: User): Observable<User> {
      return of(dummyUser)
    };
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      providers: [{
        provide: UserService,
        useClass: FakeUserService
      }],
      declarations: [ConsentFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    // act
    fixture.detectChanges();
    // expect
    expect(component).toBeTruthy();
  });

  it('validate consent selected method', () => {
    // initialise data
    const sampleUser = {
      fullName: 'Rama',
      email: 'rama@abc.com',
      consents: [{
        id: 1, description: 'test', checked: true
      }]
    };

    // act
    fixture.detectChanges();
    component.user = sampleUser;
    component.checkSelected();

    // check expected
    expect(component.selectedCheckboxes).toEqual(sampleUser.consents);
  });

  it('validate submit method', () => {
    // initialise data
    const sampleUser = {
      fullName: 'Rama',
      email: 'rama@abc.com',
      consents: [{
        id: 1, description: 'test', checked: true
      }]
    };
    const fakeUserService = fixture.debugElement.injector.get(UserService);
    const addUserSpy = spyOn(fakeUserService, 'addUser').and.callThrough();

    // act
    fixture.detectChanges();
    component.user = sampleUser;
    component.onSubmit({value: sampleUser, valid: true});

    // check expected
    expect(fakeUserService.addUser).toHaveBeenCalled();
    expect(addUserSpy).toHaveBeenCalled();
    expect(addUserSpy.calls.any()).toBeTruthy();
  });
});
