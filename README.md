# ConsentCollector

The project is used to collect consents from the user. The main page contains a from where users are supposed to enter their details and click on the 'Give consent' button to save their details.

Fields in the form.
* Full Name: full name of the user, it is required.
* email: email address of the user , it should be valid and required.
* User should click on any one of the checkboxes to enable the 'Give consent' button.

Once the user saves his data the form will be refreshed, he can go to consent details page to see all the user who have given their consents.

Consent details page will conatin the users in a table and page has pagination of better usability and performance.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

We haveuse json server for faking the APIs, The data will be stored locally in a file (db.json). To start the API, you have to run `json-server --watch db.json`.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
